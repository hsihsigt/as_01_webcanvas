
var lastX=0;
var lastY=0;

var newX=0;
var newY=0;
var canvas,
    context,
    dragging = false,
    snapshot;

var selectcolor;
color.onchange = function(){
    selectcolor = this.value;
}

var selectwidth=10;
brushsize.onchange = function(){
    selectwidth = this.value;
}

var selectfont = "Impact";
textfont.onchange = function(){
    console.log("changefont");
    selectfont = this.value;
    console.log(selectfont);
}

canvas = document.getElementById("tutorial");
context = canvas.getContext('2d');
context.strokeStyle = selectcolor;
context.fillStyle = selectcolor;
context.lineWidth = selectwidth;
context.lineCap = 'round';

canvas.addEventListener('mousedown', dragStart);
canvas.addEventListener('mousemove', drag);
canvas.addEventListener('mouseup', dragStop);




function dragStart(event) {
    if(step == -1)
        Push();
    dragging = true;
    [lastX, lastY] = [event.offsetX, event.offsetY];
    takeSnapshot();
}

function drag(event) {
    var position;
    if (dragging === true) {
        draw(event);
    }
}

function dragStop(event) {
    Push();
    dragging = false;
    draw(event);
    console.log("push");
}



function takeSnapshot() {
    snapshot = context.getImageData(0, 0, canvas.width, canvas.height);
}

function restoreSnapshot() {
    context.putImageData(snapshot, 0, 0);
}

function drawBrush(event) {
    console.log("drawfree");
    context.strokeStyle = selectcolor;
    context.fillStyle = selectcolor;
    context.lineWidth = selectwidth;
    context.globalCompositeOperation = 'source-over';
    context.save();
    context.beginPath();
    context.moveTo(lastX, lastY);
    context.lineTo(event.offsetX, event.offsetY);
    context.stroke();
    [lastX, lastY] = [event.offsetX, event.offsetY]
    context.closePath();
    context.stroke();
    context.restore();
    
    console.log("complete");
}

function drawEraser(event) {
    console.log("draweraser");
    context.strokeStyle = selectcolor;
    context.fillStyle = selectcolor;
    context.lineWidth = selectwidth;
    context.globalCompositeOperation = 'destination-out';
    context.save();
    context.beginPath();
    context.moveTo(lastX, lastY);
    context.lineTo(event.offsetX, event.offsetY);
    context.stroke();
    [lastX, lastY] = [event.offsetX, event.offsetY]
    context.closePath();
    context.stroke();
    console.log("complete");
}

function drawLine(event) {
    context.strokeStyle = selectcolor;
    context.fillStyle = selectcolor;
    context.lineWidth = selectwidth;
    context.globalCompositeOperation = 'source-over';
    context.save();
    context.beginPath();
    context.moveTo(lastX, lastY);
    context.lineTo(event.offsetX, event.offsetY);
    restoreSnapshot();
    context.stroke();
}

function drawCircle(event) {
    context.strokeStyle = selectcolor;
    context.fillStyle = selectcolor;
    context.lineWidth = selectwidth;
    context.globalCompositeOperation = 'source-over';
    context.save();
    context.beginPath();
    var radius = Math.sqrt(Math.pow((event.offsetX-lastX), 2) + Math.pow((event.offsetY-lastY), 2));
    context.arc(lastX, lastY, radius, 0, 2 * Math.PI, false);
    context.closePath();
    if (fillBox.checked) {
        restoreSnapshot();
        context.fill();
    } else {
        restoreSnapshot();
        context.stroke();
    }
}

function drawRectangle(event) {
    context.strokeStyle = selectcolor;
    context.fillStyle = selectcolor;
    context.lineWidth = selectwidth;
    context.globalCompositeOperation = 'source-over';
    context.save();
    context.beginPath();
    context.rect(lastX, lastY, event.offsetX-lastX, event.offsetY-lastY);
    context.closePath();
    console.log(lastX, lastY, newX, newY);
    if (fillBox.checked) {
        restoreSnapshot();
        context.fill();
    } else {
        restoreSnapshot();
        context.stroke();
    }
}

function drawTriangle(event) {
    context.strokeStyle = selectcolor;
    context.fillStyle = selectcolor;
    context.lineWidth = selectwidth;
    context.globalCompositeOperation = 'source-over';
    context.save();
    context.beginPath();
    context.moveTo(lastX, lastY);
    context.lineTo(lastX-(event.offsetX-lastX),event.offsetY);
    context.lineTo(event.offsetX, event.offsetY);
    context.closePath();
    console.log(lastX, lastY, newX, newY);
    if (fillBox.checked) {
        restoreSnapshot();
        context.fill();
    } else {
        restoreSnapshot();
        context.stroke();
    }
}

function draw(event) {

    var fillBox = document.getElementById("fillBox")
        shape = document.querySelector('input[type="radio"][name="shape"]:checked').id;
    
    if(shape === "brush")
    {
        document.getElementsByTagName("body")[0].style.cursor = "url('./icon/mousebrush.png'), auto";
        drawBrush(event);
    }
    if(shape === "eraser")
    {
        document.getElementsByTagName("body")[0].style.cursor = "url('./icon/mouseeraser.png'), auto";
        drawEraser(event);
    }
    if (shape === "circle") 
    {
        document.getElementsByTagName("body")[0].style.cursor = "url('./icon/mousecircle.png'), auto";
        drawCircle(event);
    }
    if (shape === "line") {
        document.getElementsByTagName("body")[0].style.cursor = "url('./icon/mouseline.png'), auto";
        drawLine(event);
    }

    if (shape === "rectangle") {
        document.getElementsByTagName("body")[0].style.cursor = "url('./icon/mouserectangle.png'), auto";
        drawRectangle(event);
    }

    if (shape === "triangle") {
        document.getElementsByTagName("body")[0].style.cursor = "url('./icon/mousetriangle.png'), auto";
        drawTriangle(event);
    }
    
}

function reset() {
    context.save();
    context.setTransform(1, 0, 0, 1, 0, 0);
    context.clearRect(0, 0, 1080, 540);
    Push();
}

var imageLoader = document.getElementById('imageLoader');
    imageLoader.addEventListener('change', handleImage, false);


function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            canvas.width = img.width;
            canvas.height = img.height;
            context.drawImage(img,0,0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);     
}

function save() {
    let imgUrl = canvas.toDataURL("image/png");
    let saveA = document.createElement("a");
    document.body.appendChild(saveA);
    saveA.href = imgUrl;
    var Today = new Date;
    saveA.download = "webcanvas_" + Today.getFullYear() + (Today.getMonth()+1) + Today.getDate() + Today.getHours() + Today.getMinutes()+ Today.getSeconds() ;
    saveA.target = "_blank";
    saveA.click();
};


function text() {
    var showtext = document.getElementById('showtext').value;
    var textsize = Math.sqrt(selectwidth)*10;
    textfontvalue = document.getElementById("textfont").value;
    context.fillStyle = selectcolor;
    context.font = textsize + "pt" + " "+ selectfont;
    context.fillText(showtext, 200 ,200);
    Push();
    console.log(selectfont);
}

let step = -1;
let userhistory = [];

     

function Push() {
    step++;
    if (step < userhistory.length-1) {
    userhistory.length = step + 1
    }
    userhistory.push(canvas.toDataURL());
}                  

function Undo() {
    if (step > 0) {
        step--;
        let canvaspic = new Image(); 
        canvaspic.src = userhistory[step]; 
        canvaspic.onload = function() {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(canvaspic, 0, 0); 
        }
    }
    
}    

function Redo() {
    if (step < userhistory.length-1) {
        step++;
        const canvasPic = new Image();
        canvasPic.src = userhistory[step];
        canvasPic.onload = function () { 
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(canvasPic, 0, 0); }
    }
}    

