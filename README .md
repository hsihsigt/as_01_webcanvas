# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets** | **Score** | **Check** |
| ------------------------ |:---------:|:---------:|
| 1.Draw straight lines    |           |           |
2.Fill the shape with color                                  | 1~5%     | Y         |


---

### How to use 
![](https://i.imgur.com/ff4QJtx.jpg)

**Icons on the top left:**

![](https://i.imgur.com/xklFDSo.png): click the checkbox to fill color in the shape you draw

![](https://i.imgur.com/jP4HYuX.png): click to use the brush

![](https://i.imgur.com/4AmkRB9.png): click to use the eraser

![](https://i.imgur.com/BCQOaZt.png): click to draw staight lines

![](https://i.imgur.com/XWNWHoK.png): click to draw a circle

![](https://i.imgur.com/A55C7o4.png): click to draw a rectangle

![](https://i.imgur.com/7GYFhiH.png): click to draw a triangle

**Icon on the right side:**

![](https://i.imgur.com/zxTuRqW.png): click to undo

![](https://i.imgur.com/tPX5x11.png): click to redo

![](https://i.imgur.com/TwmLqBA.png): click to reset the canvas

![](https://i.imgur.com/Ma0Vulf.png): click to upload a picture on your computer

![](https://i.imgur.com/aOWLxsC.png): click to download the current picture on the canvas and name the picture by  the date and time

**Others**
* click the palette to select a color you want

* drag the slider to decide the size of the text, the bush and the eraser

* delete the words in the text bar and type some words, press the enter button to display it on the canvas









    


### Function description

![](https://i.imgur.com/AYPWU3I.png): click to draw straight lines

![](https://i.imgur.com/9SOhJNK.png): check to fill the current color in a shape you're goint to draw




### Gitlab page link

https://hsihsigt.gitlab.io/as_01_webcanvas"

### Others (Optional)



<style>
table th{
    width: 100%;
}
</style>